addSbtPlugin("ch.epfl.scala"    % "sbt-scalafix" % "0.10.4")
addSbtPlugin("org.scalameta"    % "sbt-scalafmt" % "2.5.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates"  % "0.6.3")
addSbtPlugin("ch.epfl.scala"    % "sbt-bloop"    % "1.5.6")
