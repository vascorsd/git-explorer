lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .settings(
    publish / skip := true
  )
  .aggregate(
    bin_cli,
    lib_core
  )

addCommandAlias("runCli", "bin_cli / run")

lazy val bin_cli = project
  .in(file("bin/cli"))
  .dependsOn(lib_core)
  .settings(commonSettings)
  .settings(
    libraryDependencies ++=
      libraries.cats ++
        libraries.decline ++
        libraries.woof
  )

lazy val lib_core = project
  .in(file("lib/core"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++=
      libraries.cats ++
        libraries.catsParse ++
        libraries.fs2 ++
        libraries.scalaTest ++
        libraries.circe ++
        libraries.doobie ++
        libraries.woof
  )

lazy val commonSettings = Seq(
  // obviously the only sane default is to always fork execution
  fork := true,
  // if running thing asks for input we can give it inside sbt shell
  run / connectInput := true,
  // and for things that we are running then make sure the working
  // dir is the root folder
  Compile / run / baseDirectory := file("."),
  // default, nothing is publishable
  publish / skip := true,
  // saner compiler settings

  // format: off
  scalacOptions ++= Seq(
    // basic flags
    "-encoding", "utf8",
    "-deprecation", "-feature", "-unchecked",
    "-source:3.3",
    // extra info
    "-explain",
    "-explain-types",
    // new scala 3 language styling
    "-new-syntax",
    "-no-indent",
    // make warns into errors
    "-Xfatal-warnings",
    // warns / lints
    "-Wvalue-discard",
    //"-Wnonunit-statement", doesn't work yet?? - 3.3.1-RC1 ONLY
    "-Wunused:all",
  )
  // format: on
)

// libraries dependencies
lazy val libraries =
  new {
    val cats = {
      val coreVersion   = "2.9.0"
      val effectVersion = "3.5.0"

      Seq(
        "org.typelevel" %% "cats-core"   % coreVersion,
        "org.typelevel" %% "cats-effect" % effectVersion
      )
    }

    val catsParse = {
      val version = "0.3.9"

      Seq(
        "org.typelevel" %% "cats-parse" % version
      )
    }

    val fs2 = {
      val version = "3.7.0"

      Seq(
        "co.fs2" %% "fs2-core" % version,
        "co.fs2" %% "fs2-io"   % version
      )
    }

    val doobie = {
      val version = "1.0.0-RC1"

      Seq(
        "org.tpolecat" %% "doobie-core" % version,
        "org.xerial"    % "sqlite-jdbc" % "3.42.0.0",
      )
    }

    val scalaTest = {
      val testVersion   = "3.2.16"
      val effectVersion = "1.5.0"

      Seq(
        "org.scalatest" %% "scalatest"                     % testVersion   % Test,
        "org.typelevel" %% "cats-effect-testing-scalatest" % effectVersion % Test
      )
    }

    val decline = {
      val version = "2.4.1"

      Seq(
        "com.monovore" %% "decline"        % version,
        "com.monovore" %% "decline-effect" % version
      )
    }

    val woof = {
      val version = "0.6.0"

      Seq(
        "org.legogroup" %% "woof-core" % version
      )
    }

    val circe = {
      val version = "0.14.5"

      Seq(
        "io.circe" %% "circe-core"    % version,
        "io.circe" %% "circe-generic" % version,
        "io.circe" %% "circe-parser"  % version
      )
    }
  }

// # Sanity:
// really do not want supershell
Global / useSuperShell := false

// bloop generators to download sources for navigation
Global / bloopExportJarClassifiers := Some(Set("sources"))

// this one ensures we see the application output without being
// mixed with the sbt logging nonsense
Global / outputStrategy := Some(StdoutOutput)
