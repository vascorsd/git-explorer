# License

Code in the repo intended as **AGPL3+**

# What is this?

The code presented here should allow for some basic interation with a git
repository. Main purpose is to have a simple cli application and internal 
abstractions enough to use the commands as a library if intended to.

The basic operations supported for now are just to clone a repo
locally and process and show its commit history aka log.

## Features

  * Clone a repo from a github identifier - "owner/repo_name"
  * Parse and show basic information from the log - hash, message, date, author

## Tech

Project uses scala language and focuses on the use of the FP ecosystem - cats,
cats-effects, fs2, etc.

## Build

It uses the common `sbt` tool used in scala projects. Clone this project and
having `sbt` installed:

  * compile with: `sbt compile`
  * run with: `sbt cli / run`
  * create a release binary to use standalone: `sbt release`

## TODO

Basic requirements:

  * [x] Call and read git log.
  * [x] Parse git log.
  * [x] Be able to point the command to any directory.
  * [x] Use github short url format instead of full git url for clone command.
  * [ ] Cloning at first time, but reading from cache is broken.
  * [ ] Call git clone url into a specific directory.
  * [ ] Use github api when available.
  * [ ] ...

Future interesting improvements:

  * [ ] Management of the place to keep the cloned repos.
  * [ ] git command should be able to be configured instead of relying on PATH.
  * [ ] Place to keep cloned repos should be configurable instead of relying on TMP.
  * [ ] For robustness, we should verify if git command is reachable and which 
      version is available.
  * [ ] For robustness, we should pre-check access for the dump folder, space
      available, read/write permissions.
  * [ ] For coolness factor we should be generating a graal binary.
  * [ ] Both configuration file should exist and command flags to control behaviour.

## Questions  / Improvements

  1. Command `git log` has no option to be used programatically so one has to 
     "invent" their own format and define own parsing of it. Fortunately there's
     enough options to work with it. Relying on a current default format is not
     really stable since no one knows what could change in different git releases.
     We are being explicit with a format and not trusting that all information is
     always there, so the use of `Option` type internally in all of the parts we 
     want to extract. This could be refined in the future after extended testing.
  2. About encoding `git log` says it doesn't really care about it, but one could
     assume utf8 for the most part. It seems there are options to change that 
     for some uses cases, so trusting it will work for 90% of the cases at this 
     point should be ok. It could maybe made more robust but needs going deeper 
     into the rabbit hole.
  3. Currently using NUL separators in conjunction with -z log flag and tformat
     to separate each commit info into easily readable blocks. From what I saw 
     around the most people seem to invent their own string separators or other 
     things, which seems a problem waiting to happen since we enter into 
     escaping territories and special interpretation of strings, which any 
     commit message could potentialy be crafted to break the code.
  4. Parsing could be achieved in multiple ways, there's regex, basic parsing
     and split functionality in the std, and multiple libs like scodec, atto, 
     etc. I believe using a parsing library is the right approach since the thing
     we are reding may not be trivial, it pays off in the long term mastering the 
     library, maitainability of the code and potential required adjustments are
     easier too.
  5. We have access to a fs2 stream for the output of a process call, but are 
     not really taking advantage of the stream capabilities to not explode the
     memory currently since the results are being returned as a Vector of entries.
     The smarter choice is to think about and work on a more robust solution
     properly taking advantage of the available lib/types. An idea that occurrs 
     would be to push all elements through a queue and attach something on the
     the other end to consume it...
  6. ...
