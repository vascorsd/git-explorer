package gitexplorer

import cats.*
import cats.effect.*
import cats.effect.std.Console
import cats.implicits.*

import com.monovore.decline.Opts
import com.monovore.decline.effect.CommandIOApp
import org.legogroup.woof.*

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    CommandIOApp
      .run[IO](
        name = "gitexplorer",            // todo: do not hardcode name
        header = "A simple git explorer" // todo: do I care about i18n?
      )(metaProgramOpts.orElse(mainProgramOpts), args)

  val metaProgramOpts: Opts[IO[ExitCode]] =
    Args.version
      .as(
        Console[IO].println("0.0.1").as(ExitCode.Success)
      )

  def mainProgramOpts: Opts[IO[ExitCode]] =
    Args.parse.map(program)

  def program(args: Args): IO[ExitCode] = {
    for
      given Logger[IO] <- makeLogging(args.verbosity)
      core = CoreModule[IO]()
      exitResult <- Application(core).run(args)
    yield exitResult
  }

  def makeLogging(verbosity: LogLevel): IO[DefaultLogger[IO]] = {
    given Filter =
      Filter.atLeastLevel(verbosity)

    // todo: configure formatting
    given Printer =
      ColorPrinter()

    // make sure any logging messages always goes to stderr, never to stdout
    val output = new Output[IO] {
      def output(str: String): IO[Unit] =
        outputError(str)

      def outputError(str: String): IO[Unit] =
        Console[IO].errorln(str)
    }

    DefaultLogger.makeIo(output)
  }
}
