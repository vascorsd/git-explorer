package gitexplorer

import java.nio.file.Paths

import cats.effect.*

case class Application(
    core: CoreModule[IO]
) {

  def run(args: Args): IO[ExitCode] =
    for
      reposDir <- core.fileSysOps.makeNewAtTemp("gitexplorer")
      cachedDataDir <- core.fileSysOps.makeNewOrReuse(
        args.dataPath.fold(Paths.get("."))(identity),
        "__data__"
      )
      gitRepo = GitRepo.at(s"${args.user}/${args.repo}", reposDir, cachedDataDir)
      exitResult <- gitRepo
        .fold {
          IO
            .println(
              "Passed repo address does not appear to be correct. Check command --help"
            )
            .as(ExitCode.Error)
        } { repo =>
          core.gitRepoOps
            .commits(repo, false)
            .redeemWith(
              err =>
                IO
                  .println(
                    s"""
                     |Unexpected error happened.
                     |Details:
                     |${err.getMessage}
                     |""".stripMargin
                  )
                  .as(ExitCode(2)),
              _ => IO.pure(ExitCode.Success)
            )
        }
    yield exitResult

}
