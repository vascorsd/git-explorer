package gitexplorer

import java.nio.file.Path

import cats.implicits.*

import com.monovore.decline.*
import org.legogroup.woof.LogLevel

final case class Args(
    verbosity: LogLevel,
    gitExecutablePath: Option[Path],
    reposPath: Option[Path],
    dataPath: Option[Path],
    user: String,
    repo: String,
)

object Args {

  val verbosity: Opts[LogLevel] = Opts
    .flags(
      short = "v",
      long = "verbose",
      help = "Make operation more talkative. Add more flags to make it even chattier.",
      visibility = Visibility.Partial
    )
    .withDefault(0)
    .map {
      case 0 => LogLevel.Error
      case 1 => LogLevel.Warn
      case 2 => LogLevel.Info
      case 3 => LogLevel.Debug
      case _ => LogLevel.Trace
    }

  val version: Opts[Unit] = Opts
    .flag(
      long = "version",
      short = "V",
      help = "Print the version number and exit.",
      visibility = Visibility.Partial
    )

  val gitExec: Opts[Option[Path]] = Opts
    .option[Path](
      "git-exec",
      "Configure a different git binary location. Default uses 'git' command from 'PATH'."
    )
    .orNone

  val repos: Opts[Option[Path]] = Opts
    .option[Path](
      "repos-path",
      "Configure a location to clone repos into. Default uses system 'TMP'."
    )
    .orNone

  val data: Opts[Option[Path]] = Opts
    .option[Path](
      "data-path",
      "Configure a location to keep 'git' data extracted."
    )
    .orNone

  val user: Opts[String] = Opts
    .argument[String]("user")
    .validate("Argument needs to be some text. Example: \"vascorsd\".")(_.nonEmpty)

  val repo: Opts[String] = Opts
    .argument[String]("repo")
    .validate(
      "Argument needs to be some text. Example: \"dango\"."
    )(_.nonEmpty)

  val parse: Opts[Args] = (verbosity, gitExec, repos, data, user, repo).mapN(Args.apply)
}
