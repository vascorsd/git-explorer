package gitexplorer.system

import java.nio.file.Path

import scala.util.control.NoStackTrace

import cats.effect.ExitCode

import gitexplorer.CommitInfo

trait GitCommands[F[_]] {

  /** 'git clone' command.
    *
    * Named "clones" to not conflict with jvm "clone" method.
    *
    * Calls git clone as a process passing the given parameters.
    *
    * @param cloneUrl
    *   url passed to cone command
    *
    * @param targetPath
    *   location where repo is cloned to
    */
  def clones(
      cloneUrl: String,
      targetPath: Path
  ): F[Unit]

  def log(projectDir: Path): F[Vector[CommitInfo]]
}

object GitCommands {

  case class GitCommandFailure(
      command: String,
      exitCode: ExitCode,
      msg: String
  ) extends RuntimeException(s"""
       |git error:
       |    command = '${command}'
       |   exitCode = ${exitCode.code}
       |        msg =
       |”
       |${msg.linesIterator.map(" " + _).mkString("", "\n", "")}
       |“
       |""".stripMargin)
      with NoStackTrace

}
