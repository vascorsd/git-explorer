package gitexplorer.system

import java.nio.file.Path as JPath

import io.circe.Decoder
import io.circe.Encoder

trait FileSysOps[F[_]] {

  /** Creates a new empty folder somewhere in the system used for temporary files.
    *
    * @return
    *   Path to that magical place, like Tahiti.
    */
  def makeNewAtTemp(subDir: String): F[JPath]

  def validateReposDir(at: JPath): F[Unit]

  def makeNewOrReuse(at: JPath, subDir: String): F[JPath]

  def existsDirAt(path: JPath): F[Boolean]

  def dumpDataAt[A: Encoder](data: Vector[A], path: JPath): F[Unit]

  def loadDataAt[A: Decoder](path: JPath): F[Vector[A]]

}
