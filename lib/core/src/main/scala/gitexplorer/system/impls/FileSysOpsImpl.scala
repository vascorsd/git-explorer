package gitexplorer.system.impls

import java.nio.file.Path as JPath

import cats.Monad
import cats.effect.Async
import cats.effect.Concurrent
import cats.syntax.all.*

import fs2.Stream
import fs2.io.file.Files
import fs2.io.file.Path

import io.circe.Decoder
import io.circe.Encoder
import io.circe.parser.decode
import io.circe.syntax.*

import gitexplorer.system.FileSysOps

private[system] class FileSysOpsImpl[F[_]: Async: Concurrent]()(using
    fs: Files[F]
) extends FileSysOps[F] {

  def makeNewAtTemp(subDir: String): F[JPath] =
    fs.createTempDirectory(None, subDir, None).map(_.toNioPath)

  def validateReposDir(at: JPath): F[Unit] =
    ???

  def makeNewOrReuse(at: JPath, subDir: String): F[JPath] = {
    val dir = Path.fromNioPath(at) / subDir

    for
      existsDir <- existsDirAt(dir.toNioPath)
      _ <-
        if existsDir then Monad[F].unit
        else fs.createDirectories(dir)
    yield dir.toNioPath
  }

  def existsDirAt(path: JPath): F[Boolean] = {
    val dir = Path.fromNioPath(path)

    for
      exists      <- fs.exists(dir)
      isRead      <- fs.isReadable(dir)
      isDirectory <- fs.isDirectory(dir)
    yield exists && isRead && isDirectory
  }

  def dumpDataAt[A: Encoder](data: Vector[A], path: JPath): F[Unit] = {
    Stream
      .emits(data)
      .map(_.asJson.noSpaces)
      .intersperse("\n")
      .through(fs2.text.utf8.encode)
      .through(fs.writeAll(Path.fromNioPath(path)))
      .compile
      .drain
  }

  def loadDataAt[A: Decoder](path: JPath): F[Vector[A]] = {
    // TODO: handle decoding errors, we are trusting right now
    fs
      .readAll(Path.fromNioPath(path))
      .through(fs2.text.utf8.decode)
      .through(fs2.text.lines)
      .map(decode[A])
      .collect { case Right(r) => r }
      .compile
      .toVector
  }

}
