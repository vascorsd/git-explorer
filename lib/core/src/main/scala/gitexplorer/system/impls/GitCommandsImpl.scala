package gitexplorer.system.impls

import java.nio.file.Path
import cats.Applicative
import cats.ApplicativeThrow
import cats.effect.ExitCode
import cats.effect.kernel.Concurrent
import cats.effect.std.Console
import cats.parse.Parser
import cats.syntax.all.*
import fs2.Chunk
import fs2.Pipe
import fs2.Pure
import fs2.Stream
import fs2.io.process.ProcessBuilder
import fs2.io.process.Processes
import gitexplorer.CommitInfo
import gitexplorer.system.GitCommands
import gitexplorer.system.GitCommands.GitCommandFailure
import org.legogroup.woof.{*, given}

private[system] class GitCommandsImpl[F[_]]()(using
    ApplicativeThrow[F],
    Console[F],
    Logger[F],
    Concurrent[F],
    Processes[F]
) extends GitCommands[F] {

  /** git clone command, names clones to not conflict with jvm clones method. Calls git
    * clone as a process passing the given parameters.
    *
    * @param cloneUrl
    *   url passed to cone command
    * @param targetPath
    *   location where repo is cloned to
    */
  def clones(cloneUrl: String, targetPath: Path): F[Unit] = {

    val processDesc = ProcessBuilder(
      "git",
      "clone" ::
        cloneUrl ::
        targetPath.toString ::
        Nil
    )

    processDesc.spawn.use { process =>
      val exitF: F[ExitCode] = process.exitValue.map(ExitCode(_))
      val errF: F[String] = process.stderr
        .through(fs2.text.utf8.decode)
        .evalTap(s => Logger[F].trace(s"git clone (stderr): ${s}"))
        .compile
        .string

      val outF: F[Unit] = process.stdout
        .through(fs2.text.utf8.decode)
        .evalTap(s => Logger[F].trace(s"git clone (stdout): ${s}"))
        .compile
        .drain

      (exitF, outF, errF).flatMapN { (exit, out, err) =>
        if exit == ExitCode.Success then {
          Applicative[F].unit
        } else {
          ApplicativeThrow[F]
            .raiseError(
              GitCommandFailure(
                (processDesc.command :: processDesc.args).mkString(" "),
                exit,
                err
              )
            )
        }
      }

    }
  }

  def log(projectDir: Path): F[Vector[CommitInfo]] = {
    // terminator for each commit data is NUL (-z) working together with
    // "tformat" instead of "format".
    //
    // %H  - full commit hash
    // %at - author date, unix time
    // %cn - committer name
    // %B  - raw full message (subject and body not separated)
    //
    // %[space]placeholder - the space guarantees that if the placeholder exists we for sure
    //                       have a space first and then a non empty string until.

    val processDesc = ProcessBuilder(
      "git",
      "-C" ::
        projectDir.toString ::
        "log" ::
        "-z" ::
        "--pretty=tformat:hash:% H%ntime:% at%ncommitter:% cn%nmessage:% B" ::
        Nil
    )

    def splitOnNUL: Pipe[F, Byte, Chunk[Byte]] =
      _.split(_ == 0x00)

    def intoText(chunk: Chunk[Byte]): Stream[Pure, String] =
      Stream
        .chunk(chunk)
        .through(fs2.text.utf8.decode)

    def parseCommitInfo: Pipe[F, String, Either[Parser.Error, CommitInfo]] =
      _.map(CommitInfo.Parser.commitInfo.parseAll)

    def reportParsingErrs: Pipe[F, Either[Parser.Error, CommitInfo], CommitInfo] = {
      _.evalTap { (res: Either[Parser.Error, CommitInfo]) =>
        res
          .fold(
            (e: Parser.Error) => Logger[F].error(s"Found error parsing log: ${e}"),
            _ => Applicative[F].unit
          )
      }.collect { case Right(v) => v }
    }

    processDesc.spawn.use { process =>
      val exitF: F[ExitCode] = process.exitValue.map(ExitCode(_))
      val errF: F[String] = process.stderr
        .through(fs2.text.utf8.decode)
        .evalTap(s => Logger[F].trace(s"git log (stderr): ${s}"))
        .compile
        .string

      val outF: F[Vector[CommitInfo]] = process.stdout
        .through(splitOnNUL)
        .flatMap(intoText)
        .evalTap(s => Logger[F].trace(s"git log (stdout): ${s}"))
        .through(parseCommitInfo)
        .through(reportParsingErrs)
        .compile
        .toVector

      (exitF, outF, errF).flatMapN { (exit, out, err) =>
        if exit == ExitCode.Success then {
          out.pure
        } else {
          ApplicativeThrow[F]
            .raiseError(
              GitCommandFailure(
                (processDesc.command :: processDesc.args).mkString(" "),
                exit,
                err
              )
            )
        }
      }

    }
  }

}
