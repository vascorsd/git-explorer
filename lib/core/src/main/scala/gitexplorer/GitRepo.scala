package gitexplorer

import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Base64

final case class GitRepo private (
    url: String,
    savedAt: Path,
    cachedAt: Path
)

object GitRepo {
  // TODO: extended validation, ensure url string is proper format
  def at(
      repo: String,
      savedAt: Path,
      cachedDataDir: Path
  ): Option[GitRepo] = {
    def expandToFullGitHubUrl(repo: String) =
      s"https://github.com/${repo}.git"

    val url       = expandToFullGitHubUrl(repo)
    val fsRepoKey = Base64.getEncoder.encodeToString(url.getBytes(StandardCharsets.UTF_8))

    Option(
      GitRepo(
        url,
        Paths.get(savedAt.toString, fsRepoKey),
        Paths.get(cachedDataDir.toString, fsRepoKey)
      )
    )
  }
}
