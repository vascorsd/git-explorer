package gitexplorer

import io.circe.*

import gitexplorer.CommitInfo.*

final case class CommitInfo(
    hash: Hash,
    date: AuthorDate,
    author: CommitterName,
    message: Message
) derives Codec.AsObject

object CommitInfo {

  /** Full git commit hash. At the current time a SHA1 hash string */
  final case class Hash(
      v: Option[String]
  ) derives Codec.AsObject

  /** Author Date - unix time stamp */
  final case class AuthorDate(
      v: Option[Long]
  ) derives Codec.AsObject

  final case class CommitterName(
      v: Option[String]
  ) derives Codec.AsObject

  /** Full raw message text, includes both subject and body as originally written */
  final case class Message(
      v: Option[String]
  ) derives Codec.AsObject

  object Parser {

    import cats.parse.*
    import cats.parse.Parser.*
    import cats.parse.Rfc5234 as rfc

    def tag(name: String): Parser[String] = {
      (string(name) ~ char(':')).string
    }

    def tagLine(tagName: String): Parser[Option[String]] = {
      val value = (rfc.sp *> anyChar.repUntil(rfc.lf).string).? <* rfc.lf
      tag(tagName) *> value
    }

    def commitBody: Parser[Option[String]] = {
      val value = (rfc.sp *> anyChar.rep.string).?
      tag("message") *> value
    }

    val commitInfo: Parser[CommitInfo] = {
      (tagLine("hash") ~
        tagLine("time").map(_.map(_.toLong)) ~
        tagLine("committer") ~
        commitBody).map { case (((h, t), c), b) =>
        CommitInfo(
          CommitInfo.Hash(h),
          CommitInfo.AuthorDate(t),
          CommitInfo.CommitterName(c),
          CommitInfo.Message(b)
        )
      }
    }
  }
}
