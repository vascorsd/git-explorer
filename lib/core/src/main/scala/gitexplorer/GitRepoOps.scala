package gitexplorer

trait GitRepoOps[F[_]] {
  def commits(repo: GitRepo, useCache: Boolean): F[Vector[CommitInfo]]
}
