package gitexplorer.impls

import java.time.Instant

import cats.Monad
import cats.effect.std.Console
import cats.syntax.all.*

import gitexplorer.CommitInfo
import gitexplorer.GitRepo
import gitexplorer.GitRepoOps
import gitexplorer.system.*

private[gitexplorer] class GitRepoOpsImpl[F[_]](
    git: GitCommands[F],
    fs: FileSysOps[F]
)(using
    Monad[F],
    Console[F]
) extends GitRepoOps[F] {

  def commits(repo: GitRepo, useCache: Boolean): F[Vector[CommitInfo]] = {
    for
      // TODO: improve where we generate target folder,
      // no need to be hardcoded. We are currently trusting in always
      // having a new clean folder to clone to.
      _ <- Console[F].println(s"Checking if we have data in cache for: ${repo.url} ...")

      maybeCached <- getCommits(repo, useCache = false)
      commits <- maybeCached.fold {
        for
          _       <- Console[F].println(s"Not cached: ${repo.url} ...")
          _       <- Console[F].println(s"Cloning repo: ${repo.url} ...")
          newData <- git.clones(repo.url, repo.savedAt) *> git.log(repo.savedAt)
          _       <- Console[F].println(s"Cloned ?? ...")
          _       <- storeCommitInfo(repo, newData)
          _       <- Console[F].println(s"Stored ?? ...")
        yield (newData)
      } { cached =>
        for _ <- Console[F].println(s"Found cached data for: ${repo.url} ...")
        yield (cached)
      }
      _ <- prettyPrint(commits).sequence
    yield (commits)
  }

  private def prettyPrint(commits: Vector[CommitInfo]) = {
    commits.map { ci =>
      Console[F].println(prettyPrintCommit(ci)) *>
        Console[F].println("---- ---- ----")
    }
  }

  private def prettyPrintCommit(ci: CommitInfo) = {
    def prettyDate(unixTime: Long) =
      Instant.ofEpochSecond(unixTime).toString

    s"""hash: ${ci.hash.v.fold("")(identity)}
       |by: "${ci.author.v.fold("")(identity)}"
       |at: ${ci.date.v.fold("")(prettyDate)}
       |message:
       |${ci.message.v.fold("")(identity)}
       |""".stripMargin
  }

  private def storeCommitInfo(repo: GitRepo, commits: Vector[CommitInfo]) = {
    fs.dumpDataAt(commits, repo.cachedAt)
  }

  private def getCommits(repo: GitRepo, useCache: Boolean) = {
    for
      exists <- fs.existsDirAt(repo.cachedAt)
      savedData <-
        if useCache & exists then {
          readStoredCommitInfo(repo).map(_.some)
        } else {
          Monad[F].pure(none[Vector[CommitInfo]])
        }
    yield (savedData)
  }

  private def readStoredCommitInfo(repo: GitRepo) = {
    fs.loadDataAt[CommitInfo](repo.cachedAt)
  }
}
