package gitexplorer

import cats.effect.Async
import cats.effect.std.Console

import fs2.io.file.Files
import fs2.io.process.Processes

import org.legogroup.woof.*

import gitexplorer.impls.*
import gitexplorer.system.*
import gitexplorer.system.impls.*

case class CoreModule[F[_]]()(using
    Async[F],
    Console[F],
    Logger[F],
    Processes[F],
    Files[F]
) {
  val gitCommands: GitCommands[F] = GitCommandsImpl()
  val fileSysOps: FileSysOps[F]   = FileSysOpsImpl()
  val gitRepoOps: GitRepoOps[F]   = GitRepoOpsImpl(gitCommands, fileSysOps)
}
