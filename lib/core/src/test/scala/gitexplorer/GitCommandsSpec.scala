package gitexplorer

import java.nio.file.Paths
import cats.effect.*
import cats.effect.testing.scalatest.AsyncIOSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import gitexplorer.system.impls.GitCommandsImpl
import org.legogroup.woof.*

class GitCommandsSpec extends AsyncWordSpec with AsyncIOSpec with Matchers {
  private def project(name: String) = {
    val projPath = Paths.get(s"../../test-samples/${name}")
    projPath.toAbsolutePath.normalize
  }

  object TestLogging {
    class NoOutput() extends Output[IO] {
      def output(str: String): IO[Unit] =
        IO.unit
      def outputError(str: String): IO[Unit] =
        IO.unit
    }

    given Filter =
      Filter.everything

    given Printer =
      NoColorPrinter()

    val logger: IO[Logger[IO]] = {
      DefaultLogger.makeIo(NoOutput())
    }
  }

  "git log" should {

    "not break & properly handle - repo with empty message" in {
      for {
        given Logger[IO] <- TestLogging.logger

        _ <- GitCommandsImpl[IO]
          .log(
            project("/repo-commit-with-empty-msg")
          )
          .asserting { gr =>
            gr shouldBe {
              Vector(
                CommitInfo(
                  CommitInfo.Hash(Some("f5f49ad1c66195f0f1f1edf90d720a858cca4517")),
                  CommitInfo.AuthorDate(Some(1631281868)),
                  CommitInfo.CommitterName(Some("Vasco Dias")),
                  CommitInfo.Message(None)
                )
              )
            }
          }
      } yield ()
    }

    "not break & properly handle - repo with different Author & Committer with emojis text" in {
      for {
        given Logger[IO] <- TestLogging.logger

        _ <- GitCommandsImpl[IO]
          .log(
            project("/repo-different-author-committer")
          )
          .asserting { gr =>
            gr shouldBe {
              Vector(
                CommitInfo(
                  CommitInfo.Hash(Some("9baa6da5b283d9a00254a8ed9489b392b44dc297")),
                  CommitInfo.AuthorDate(Some(1631281776)),
                  CommitInfo.CommitterName(Some("\uD83C\uDF49charmander")),
                  CommitInfo.Message(
                    Some(
                      """commit using env variables GIT_AUTHOR_NAME and GIT_COMMITTER_NAME to change author and commiter and even put smileys into it
                        |""".stripMargin
                    )
                  )
                )
              )
            }
          }
      } yield ()
    }

    "not break & properly handle - seemingly normal repo with single commit and special char in message" in {
      for {
        given Logger[IO] <- TestLogging.logger

        _ <- GitCommandsImpl[IO]
          .log(
            project("/repo-1commit")
          )
          .asserting { gr =>
            gr shouldBe {
              Vector(
                CommitInfo(
                  CommitInfo.Hash(Some("726f72554d87ed81fc4ad0862089b70d79ff9d26")),
                  CommitInfo.AuthorDate(Some(1631281248)),
                  CommitInfo.CommitterName(Some("Vasco Dias")),
                  CommitInfo.Message(
                    Some(
                      """\n
                        |""".stripMargin
                    )
                  )
                )
              )
            }
          }
      } yield ()
    }

  }
}
